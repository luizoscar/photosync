# Python File Synchronizer

This Python application was developed with the purpose of facilitating the process of copying photos and videos from digital cameras, to an external HD.  
The application will find files of the same name and size in the destination sub-directories, thus avoiding unnecessary copying of files that have been previously synchronized.  

The application allows the copied video files to be re-encoded in other formats, saving some disk space.  
Conversion of videos is done using the open source tool [ffmpeg](https://www.ffmpeg.org/).  

**Note:**  
In order to support the H265 Codec (recommended codec), it may be necessary to recompile ffmpeg and enabling the H265 codec flag, check the steps described in
 [ffmpeg compilation guide](https://trac.ffmpeg.org/wiki/CompilationGuide).

## Usage

The application basically consists of 2 screens:  

### Main screen

The application main screen, where the user can specify the source and destination directories and open the configuration screen.  

![alt text](imagens/main_window.png "Application main screen")

After selecting the source and destination directories, the user must click **Refresh**, where the application will search the files in the source and destination directory tree, comparing each of the files by name and size.  
The application will display a grid with the file list, the file type, the file size and a check box, allowing the selection for copying.  
After selecting the files to be copied, the user must click the **Synchronize** and the application will copy the files.  

If the video conversion option is selected, the application will convert any copied video to the selected format.  

### Application settings window.

Screen used to specify the application parameters.  

![alt text](imagens/config_window.png "Application settings")

### Directory Mapping Window.

This functionality allows the user to map the destination sub-directories, where the files will be copied, making it possible to make corrections in case the files were created with the wrong date, for example.  

![alt text](imagens/mapping_window.png "Directory Mapping Window")

## Prerequisites for running the application

For the complete operation of the application it is necessary that the following tools are installed in the system:  

1. Python `2.7` or `3.5`.
2. Python libraries: LXML, future and GTK3.
3. Ffmpeg media converter in system path or configured in `settings.xml` file.  

## Installation of Requirements

### Linux

1 - Install Python, PyPI and ffmpeg:

```sh
sudo apt-get install python-pip ffmpeg python-gi
```

2 - Using python-pip, install the `lxml` and` future` library:

```sh
sudo pip install lxml future
```

**Note:**  
In Linux (ubuntu) you will not need to install GTK3.  

### Windows

1 - Install [Python 2.7](https://www.python.org/downloads/release/python-2713/) or [Python 3.5](https://www.python.org/downloads/release/python-363/).

**Warning:**  
During the installation, remember to check the option to add Python to the System Path or manually add the PATH environment variable.  

- Python Installation

![alt text](imagens/windows_python.png "Python 2.7 Installation")

2 - Install [PyGObject for Windows](https://sourceforge.net/projects/pygobjectwin32/files/pygi-aio-3.18.2_rev12-setup_549872deadabb77a91efbc56c50fe15f969e5681.exe/download)

**Warning:**  
During installation, the following components should be selected: `Base Packages` and` GTK + 3.18.9` (see images).  

- Selecting the directory where Python 2.7 is installed

![alt text](imagens/pygobject1.png "Selecting the Python 2.7 directory")

- Select the Python 2.7

**Note:**  
If the same version appears more than once, select only once.  

![alt text](imagens/pygobject2.png "Select the Python 2.7")

- Selection of basic components

![alt text](imagens/pygobject3.png "Selection of basic components")

- Selection of GTK3 +

![alt text](imagens/pygobject4.png "Selection of GTK3+")

- Ignore non-GNU packages

![alt text](imagens/pygobject5.png "Ignore non-GNU packages")

- Ignore additional packages

![alt text](imagens/pygobject6.png "Ignore additional packages")

- Proceed with the installation

![alt text](imagens/pygobject7.png "Proceed with the installation")

3 - Using python pip via Command Line, install the `lxml` and` future` library:

```sh
pip install lxml future
```
4 - Install [ffmpeg](https://www.ffmpeg.org/download.html) and add it to the system path or configure it in `settings.xml` file.
