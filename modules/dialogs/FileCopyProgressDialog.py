# -*- coding: utf-8 -*-

'''
###############################################################################################'
 FileCopyProgressDialog.py

 This is a free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This file is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with the software; see the file COPYING. If not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

###############################################################################################'
 @class Dialog used to display the file copy operation 
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''
from __future__ import division
import shutil
import os
from threading import Thread

from gi.repository import Gtk, GLib

from modules.dialogs.BaseContainer import BaseDialog
from modules.settings.AppSettings import AppSettings


class FileCopyProgressDialog(BaseDialog):
    """
    Dialog used to display the operation progress
    """
    
    _must_stop = False
    _failed = False
    _total = 0
    _completed_size = 0

    def __init__(self, parent, file_list, destination, file_mapping):
        
        super(FileCopyProgressDialog, self).__init__(title="Copying the file list", transient_for=parent)
        self.add_button(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL)

        self.set_size_request(250, 150)
        self.set_border_width(10)
        self.file_list = file_list
        self.destination_dir = destination
        self.file_mapping = file_mapping

        # Main container
        grid = Gtk.Grid()
        grid.set_column_homogeneous(True)
        grid.set_row_homogeneous(True)
        grid.set_column_spacing(4)
        grid.set_row_spacing(6)

        for file_name in self.file_list:
            self._total = self._total + os.stat(file_name).st_size

        # Label with the total file progress
        grid.attach(Gtk.Label(label="Copying " + str(len(file_list)) + 
                              " files (" + self.get_utils().to_human_size(self._total) + ")", halign=Gtk.Align.START), 0, 0, 6, 1)

        # Global progress bar
        self.progress_bar = Gtk.ProgressBar(show_text=True)
        grid.attach(self.progress_bar, 0, 1, 6, 1)

        # File progress label
        self.label_progress = Gtk.Label(halign=Gtk.Align.START)
        grid.attach(self.label_progress, 0, 2, 6, 1)

        self.get_content_area().pack_start(grid, True, True, 0)
        self.show_all()

        thread = Thread(target=self._do_copy_files)
        thread.daemon = True
        thread.start()

    def _do_update_progess(self, progress_title, progress_current_file, title_current_file):
        """
        Update the copy progress 
        """
        self.progress_bar.set_fraction(progress_current_file)  # Progress must be between 0.0 and 1.0
        self.progress_bar.set_text(progress_title)
        self.label_progress.set_text(title_current_file)
        return False

    def _do_copy_files(self):
        """
        Copy the files
        """
        total_files = len(self.file_list)
        self._completed_size = 0
        for i, file_name in enumerate(self.file_list):
            try:
                self._completed_size = self._completed_size + os.stat(file_name).st_size

                progress_title = "[" + self.get_utils().to_human_size(self._completed_size) + "/" + self.get_utils().to_human_size(self._total) + "]"
                progress_total = self._completed_size / self._total  # Progress
                
                current_file_title = "[" + str(i) + "/" + str(total_files) + "] " + os.path.basename(file_name) + " (" + self.get_utils().to_human_size(os.stat(file_name).st_size) + ")"

                GLib.idle_add(self._do_update_progess, progress_title, progress_total, current_file_title)

                # Check if interrupted by user
                if self._must_stop:
                    return None

                # Create missing destination dir
                new_file_name = self.destination_dir + os.sep + self.file_mapping.get_file_destination(file_name)
                new_file_dir = os.path.dirname(new_file_name)
                if not os.path.exists(new_file_dir):
                    try:
                        self.debug("Creating directory " + new_file_dir)
                        os.makedirs(new_file_dir)
                    except Exception as e:
                        self.debug("Failed to create the destination dir [" + new_file_dir + "]: " + str(e))
                        continue

                # Copy the file
                self.debug("Copy file " + file_name + " -> " + new_file_name)
                shutil.copy2(file_name, new_file_name)

                # Delete the source, if required
                if 'True' == self.get_app_settings(AppSettings.CFG_REMOVE_AFTER_COPY):
                    try:
                        self.debug("Removing source file " + file_name)
                        os.remove(file_name)
                    except Exception as e:
                        self.debug("Failed to remove the source file after the copy [" + file_name + "]: " + str(e))
            except Exception as e:
                self.debug("Failed to copy the file [" + file_name + "]: " + str(e))
                continue

        self.close()
