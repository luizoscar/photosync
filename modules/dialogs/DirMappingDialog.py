# -*- coding: utf-8 -*-

'''
###############################################################################################'
 DirMappingDialog.py

 This is a free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This file is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with the software; see the file COPYING. If not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

###############################################################################################'
 @class Dialog used to map the destination directory 
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''
from gi.repository import Gtk

from modules.dialogs.BaseContainer import BaseDialog


class DirMappingDialog(BaseDialog):
    """
    Dialog used to map the destination directories
    """
    def __init__(self, parent, file_mapping):

        super(DirMappingDialog, self).__init__(title="Destination dir mapping", transient_for=parent)
        self.add_button(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL)
        self.add_button(Gtk.STOCK_OK, Gtk.ResponseType.OK)

        self.set_size_request(500, 400)
        self.set_border_width(10)

        self.file_mapping = file_mapping
        
        scrolledwindow = Gtk.ScrolledWindow()
        scrolledwindow.set_hexpand(True)
        scrolledwindow.set_vexpand(True)

        self.grid = Gtk.Grid()
        self.grid.attach(scrolledwindow, 0, 1, 3, 1)

        self.textview = Gtk.TextView()
        scrolledwindow.add(self.textview)

        # Load the current mapping
        g_dic_mapeamento_dir_destino=  self.file_mapping.get_target_mapping()
        g_dic_mapeamento_dir_origem = self.file_mapping.get_source_mapping()
        
        lines = ""
        
        for key in sorted(g_dic_mapeamento_dir_destino.iterkeys()):
            if key in g_dic_mapeamento_dir_origem:            
                lines = lines + key + " => " + g_dic_mapeamento_dir_destino[key] + "   #" + g_dic_mapeamento_dir_origem[key] + "\n"
            else:
                lines = lines + key + " => " + g_dic_mapeamento_dir_destino[key] + "\n"

        self.textview.get_buffer().set_text(lines)

        self.get_content_area().pack_start(self.grid, True, True, 0)
        self.show_all()

    def show_and_update_file_list(self):
        g_dic_mapeamento_dir_destino=  self.file_mapping.get_target_mapping()
        while self.run() == Gtk.ResponseType.OK:
            buf = self.textview.get_buffer()
            resp = buf.get_text(buf.get_start_iter(), buf.get_end_iter(), True)
            for line in resp.splitlines():
                key = line[:line.find("=>")].strip()
                value = line[line.find("=>") + 2:line.find("#")].strip() if '#' in line else line[line.find("=>") + 2:].strip()
                g_dic_mapeamento_dir_destino[key] = value

            self.destroy()
            return True

        self.destroy()
        return False
