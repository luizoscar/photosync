# -*- coding: utf-8 -*-
'''
###############################################################################################'
 MainWindow.py

 This is a free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This file is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with the software; see the file COPYING. If not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

###############################################################################################'
 @class Application main window 
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''
from gi.repository import Gtk, Gdk
from glob import glob
import re
import os
import sys
import subprocess

from modules.dialogs.BaseContainer import BaseWindow
from modules.dialogs.ConfigDialog import ConfigDialog
from modules.dialogs.TextViewerDialog import TextViewerDialog
from modules.dialogs.VideoEncodeProgressDialog import VideoEncodeProgressDialog
from modules.dialogs.DirMappingDialog import DirMappingDialog
from modules.dialogs.FileCopyProgressDialog import FileCopyProgressDialog
from modules.settings.DestinationDirSettings import DestinationDirSettings 
from modules.settings.AppSettings import AppSettings

class MainWindow(BaseWindow):
    """
    Application Main Window
    """
    
    _GRID_COLS = ["Copy", "Status", "Source", "Destination", "Type", "Size", "Details"]
    _popup_menu = Gtk.Menu()
    _destination_settings = None
    _VERSION = "v1.0"  #  Application version

    # Source file variables
    _source_loading_finished = False  # Signal the finish of source file reading thread 
    _source_file_list = None  # Source file list
    _dic_source_file_info = None  # Dictionary with the source file info

    # Destination files
    _destination_loading_finished = False  # Signals the end of the destination file listing
    _destination_file_list = None  # List of the destination files

    _app_settings = None
    
    def __init__(self):
        Gtk.Window.__init__(self, title="Photo Sync - " + self._VERSION)

        self.set_icon_name("application-x-executable")
        Gtk.Settings().set_property('gtk_button_images', True)

        # Clipboard to copy the text
        self.clipboard = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)

        self._destination_settings = DestinationDirSettings()

        self.set_resizable(True)
        self.set_border_width(10)
        self.set_default_size(640, 480)
        self.set_size_request(640, 480)

        # Main Container 
        grid = Gtk.Grid()
        grid.set_column_homogeneous(True)
        grid.set_row_homogeneous(True)
        grid.set_column_spacing(4)
        grid.set_row_spacing(6)

        # Source dir
        grid.attach(Gtk.Label(label="Source dir:", halign=Gtk.Align.START), 0, 0, 1, 1)
        self.edit_origem = Gtk.Entry()
        self.edit_origem.set_activates_default(True)
        self.edit_origem.set_text(self.get_app_settings(AppSettings.CFG_SOURCE_DIR))

        grid.attach(self.edit_origem, 1, 0, 6, 1)

        button = Gtk.Button.new_from_icon_name("folder-open", Gtk.IconSize.BUTTON)
        button.connect("clicked", self.do_click_source)
        flowbox = Gtk.FlowBox()
        flowbox.set_selection_mode(Gtk.SelectionMode.NONE)       
        flowbox.add(button)
        grid.attach(flowbox, 7, 0, 1, 1)
        self.labelStatusFrom = Gtk.Label(label="", halign=Gtk.Align.START)
        grid.attach(self.labelStatusFrom, 0, 1, 8, 1)

        # Target dir
        grid.attach(Gtk.Label(label="Target dir:", halign=Gtk.Align.START), 0, 2, 1, 1)
        self.edit_destino = Gtk.Entry()
        self.edit_destino.set_text(self.get_app_settings(AppSettings.CFG_TARGET_DIR))
        grid.attach(self.edit_destino, 1, 2, 6, 1)
        button = Gtk.Button.new_from_icon_name("folder-open", Gtk.IconSize.BUTTON)
        button.connect("clicked", self.do_click_target)
        flowbox = Gtk.FlowBox()
        flowbox.set_selection_mode(Gtk.SelectionMode.NONE)
        flowbox.add(button)

        grid.attach(flowbox, 7, 2, 1, 1)
        self.labelStatusTo = Gtk.Label(label="", halign=Gtk.Align.START)
        grid.attach(self.labelStatusTo, 0, 3, 8, 1)

        # Button bar

        # Refresh
        self.button_ler_files = self._create_icon_and_label_button("Refresh", "view-refresh", self.do_click_check_files)
        grid.attach(self.button_ler_files, 0, 4, 1, 1)

        # Sync
        self.button_sync_files = self._create_icon_and_label_button("Synchronize", "system-run", self.do_click_sync_files)
        self.button_sync_files.set_sensitive(False)
        grid.attach(self.button_sync_files, 1, 4, 1, 1)

        # File mapping
        self.button_mapeamento = self._create_icon_and_label_button("File mapping", "document-properties", self.do_click_dir_mapping)
        self.button_mapeamento.set_sensitive(False)
        grid.attach(self.button_mapeamento, 2, 4, 1, 1)

        # Settings
        self.button_config = self._create_icon_and_label_button("Settings", "applications-system", self.do_click_config)
        grid.attach(self.button_config, 3, 4, 1, 1)

        # Logs
        button = self._create_icon_and_label_button("Logs", "system-search", self.do_click_logs)
        grid.attach(button, 4, 4, 1, 1)

        # Close
        button = self._create_icon_and_label_button("Close", "window-close", self.do_click_close)
        grid.attach(button, 7, 4, 1, 1)

        # File grid

        # Create the store
        self.store = Gtk.ListStore(bool, str, str, str, str, str, str)

        self.filtro = self.store.filter_new()
        cell_renderer = Gtk.CellRendererText()

        # Add the columns
        self.treeview = Gtk.TreeView(model=self.store)
        self.treeview.connect("button_press_event", self.do_show_popup)

        # Cols 0 and 1 are not text
        col1 = Gtk.TreeViewColumn("Copy", Gtk.CellRendererToggle(), active=0)
        col1.set_sort_column_id(0)
        self.treeview.append_column(col1)

        col2 = Gtk.TreeViewColumn("Status", Gtk.CellRendererPixbuf(), icon_name=1)
        col2.set_sort_column_id(1)
        self.treeview.append_column(col2)

        # Add the remaining cols
        for i, column_title in enumerate(self._GRID_COLS):
            column = Gtk.TreeViewColumn(column_title, cell_renderer, text=i)
            if i > 1:  # Cols 0 and 1 are the checkbox and the icon(added previously)
                self.treeview.append_column(column)
            self.store.set_sort_func(i, self._compare_tree_rows, None)
            column.set_sort_column_id(i)

        self.treeview.connect("row-activated", self.on_tree_double_clicked)

        # Append the treeview to the scrollwindow
        scrollable_treelist = Gtk.ScrolledWindow()
        scrollable_treelist.set_vexpand(True)
        scrollable_treelist.add(self.treeview)
        grid.attach(scrollable_treelist, 0, 5, 8, 8)

        # File selection label
        self.label_status_copia = Gtk.Label(label="", halign=Gtk.Align.START)
        grid.attach(self.label_status_copia, 0, 13, 8, 1)

        self.add(grid)

        i0 = Gtk.MenuItem("Un-Select all files")
        i0.connect("activate", self.do_uncheck_all)
        self._popup_menu.append(i0)
        i1 = Gtk.MenuItem("Select all videos")
        i1.connect("activate", self.do_check_all_videos)
        self._popup_menu.append(i1)
        i2 = Gtk.MenuItem("Select all photos")
        i2.connect("activate", self.do_check_all_photos)
        self._popup_menu.append(i2)
        i3 = Gtk.MenuItem("Select non H265 videos")
        i3.connect("activate", self.do_check_non_h265)
        self._popup_menu.append(i3)
        i4 = Gtk.MenuItem("Delete the selected files")
        i4.connect("activate", self.do_delete_selected_files)
        self._popup_menu.append(i4)

        self._popup_menu.show_all()

    def do_show_popup(self, tv, event):  # @UnusedVariable
        if event.button == 3:
            self._popup_menu.popup(None, None, None, None, 0, Gtk.get_current_event_time())

    def do_delete_selected_files(self, widget):  # @UnusedVariable
        self.debug("MenuItem: Delete the selected files")
        files = self.do_build_list_files_to_copy()
        if len(files) > 0:
            dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.QUESTION, Gtk.ButtonsType.YES_NO, "File exclusion")
            dialog.format_secondary_text("Are you sure you want to permanently remove the " + str(len(files)) + " selected files?")
            response = dialog.run()
            if response == Gtk.ResponseType.YES:
                for filename in files:
                    self.debug("Removing file " + filename)
                    os.remove(filename)
                self.do_build_file_list()
            dialog.destroy()

    def do_check_non_h265(self, widget):  # @UnusedVariable
        self.debug("MenuItem: Select non H265 videos")
        for row in self.store:
            if self.is_video(row[2]) and 'hevc' not in row[6]:
                row[0] = True

        self.do_update_selection_counters()

    def do_check_all_photos(self, widget):  # @UnusedVariable
        self.debug("MenuItem: Select all photos")
        for row in self.store:
            if self.is_photo(row[2]):
                row[0] = True
        self.do_update_selection_counters()

    def do_check_all_videos(self, widget):  # @UnusedVariable
        self.debug("MenuItem: Select all videos")
        for row in self.store:
            if self.is_video(row[2]):
                row[0] = True

        self.do_update_selection_counters()

    def do_uncheck_all(self, widget):  # @UnusedVariable
        self.debug("MenuItem: Un-Select all files")
        for row in self.store:
            row[0] = False

        self.do_update_selection_counters()

    def do_click_source(self, widget):  # @UnusedVariable
        self.do_select_dir("source")

    def do_click_target(self, widget):  # @UnusedVariable
        self.do_select_dir("target")

    def do_select_dir(self, titulo):
        self.debug("Select the " + titulo + " directory.")

        if titulo == "source":        
            editor = self.edit_origem 
            xml_tag = AppSettings.CFG_SOURCE_DIR 
        else: 
            editor = self.edit_destino
            xml_tag = AppSettings.CFG_TARGET_DIR 

        dialog = Gtk.FileChooserDialog("Select the " + titulo + " directory.", self, Gtk.FileChooserAction.SELECT_FOLDER,
                                       (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OPEN, Gtk.ResponseType.OK))

        current_dir = editor.get_text().strip()
        if os.path.isdir(current_dir):
            dialog.set_current_folder(current_dir)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            editor.set_text(dialog.get_filename())
            self.debug("Selected " + titulo + " directory: " + dialog.get_filename())
            self.set_app_settings(xml_tag, dialog.get_filename())

        dialog.destroy()

    def get_file_type(self, filename):
        tipo = "Unknown"
        if self.is_photo(filename):
            tipo = "Photo"
        elif self.is_video(filename):
            tipo = "Video"
        return tipo

    def get_file_is_sync(self, filename):
        found = False
        if os.path.isfile(filename):
            files = self._destination_file_list.get(os.path.basename(filename), [])
            tamanho_origem = os.stat(filename).st_size
            if len(files) > 0:
                for  dests in files:
                    found = found or tamanho_origem == os.stat(dests).st_size
        return found

    def get_file_icon(self, sync):
        mover = 'True' == self.get_app_settings(AppSettings.CFG_REMOVE_AFTER_COPY)
        resp = "forward" if mover else "go-down"

        if sync:
            sobrescreve = 'True' == self.get_app_settings(AppSettings.CFG_OVERRIDE_EXISTING)
            resp = "gtk-stop" if sobrescreve else "ok"

        return resp

    def do_build_file_list(self):
        active = self._source_loading_finished and self._destination_loading_finished

        if active:
            self.debug("Loading the file grid")

            # Check if must override the files
            sobrescrever = 'True' == self.get_app_settings(AppSettings.CFG_OVERRIDE_EXISTING)

            self.store.clear()
            src = self.edit_origem.get_text().strip()

            pos_src = len(src) if src.endswith(os.sep) else len(src) + 1
            
            for filename in self._source_file_list:
                if os.path.isfile(filename):
                    sync = self.get_file_is_sync(filename)
                    icon = self.get_file_icon(sync)
                    tamanho = self.get_utils().to_human_size(os.stat(filename).st_size)
                    detalhes = self._dic_source_file_info[filename]
                    file_abr = filename[pos_src:]
                    tipo_file = self.get_file_type(filename)
                    destino = self._destination_settings.get_file_destination(filename)
    
                    # sync must be always False to override 
                    if sobrescrever:
                        sync = False
    
                    self.store.append([
                        not sync,
                        icon,
                        file_abr,
                        destino,
                        tipo_file,
                        tamanho,
                        detalhes
                    ])

            # Enable the buttons
            self.button_ler_files.set_sensitive(active)
            self.button_sync_files.set_sensitive(active)
            self.button_mapeamento.set_sensitive(active)

            # Update the counter
            self.do_update_selection_counters()
            self.debug("File grid loaded")

    def do_read_file_list_source(self):
        self._dic_source_file_info = {}
        self._source_file_list = []

        # Build the file list
        self._source_file_list = [y for x in os.walk(self.edit_origem.get_text()) for y in glob(os.path.join(x[0], '*.*'))]
        tamanho = 0
        for filename in self._source_file_list:
            try:
                # Load the file information
                self._dic_source_file_info[filename] = self.get_file_info(filename)
                tamanho = tamanho + os.stat(filename).st_size  # in bytes
            except:
                self.debug("Failed to read the file: " + filename)

        self.labelStatusFrom.set_text("Files on the source dir: " + str(len(self._source_file_list)) + " (" + self.get_utils().to_human_size(tamanho) + ")")
        self.debug(self.labelStatusFrom.get_text())
        self._source_loading_finished = True
        self.do_build_file_list()
        self.debug("Source file list loading completed!")

    def do_read_file_list_target(self):
        self._destination_file_list = {}
        file_list = [y for x in os.walk(self.edit_destino.get_text()) for y in glob(os.path.join(x[0], '*.*'))]
        tamanho = 0
        for filename in file_list:
            try:
                tamanho = tamanho + os.stat(filename).st_size  # in bytes
                nome = os.path.basename(filename)
                files = self._destination_file_list.get(nome, [])
                files.append(filename)
                self._destination_file_list[nome] = files
            except:
                self.debug("Failed to read the file: " + filename)

        self.labelStatusTo.set_text("Files on the target dir: " + str(len(file_list)) + " (" + self.get_utils().to_human_size(tamanho) + ")")
        self.debug(self.labelStatusTo.get_text())
        self._destination_loading_finished = True
        self.do_build_file_list()
        self.debug("Destination file list loading completed!")

    def do_click_check_files(self, widget):  # @UnusedVariable
        self.debug("Checking directories")

        if not os.path.isdir(self.edit_origem.get_text()):
            return self._show_message("Invalid dir", "Unable to locate the source dir.")

        if not os.path.isdir(self.edit_destino.get_text()):
            return self._show_message("Invalid dir", "Unable to locate the target dir.")

        self.debug("Checking the file list")

        self._source_file_list = []
        self._destination_file_list = {}
        self._source_loading_finished = False
        self._destination_loading_finished = False
        
        self._destination_settings.clear()

        # Disable the buttons
        self.button_ler_files.set_sensitive(False)
        self.button_sync_files.set_sensitive(False)
        self.button_mapeamento.set_sensitive(False)

        self.store.clear()

# If the performance is bad, the comparison can be made using two threads  
#         Thread(target=self.do_read_file_list_source).start()
#         Thread(target=self.do_read_file_list_target).start()

        # Compare the list of files
        self.do_read_file_list_source()
        self.do_read_file_list_target()

    def do_update_selection_counters(self):
        cont = 0
        cont_video = 0
        cont_photo = 0
        cont_outro = 0
        size = 0
        size_video = 0
        size_photo = 0
        size_outro = 0

        for row in self.store:
            if row[0]:
                filename = self.edit_origem.get_text() + os.sep + row[2]
                cont += 1
                size += os.stat(filename).st_size

                if self.is_video(filename):
                    cont_video += 1
                    size_video += os.stat(filename).st_size
                elif self.is_photo(filename):
                    cont_photo += 1
                    size_photo += os.stat(filename).st_size
                else:
                    cont_outro += 1
                    size_outro += os.stat(filename).st_size

        self.label_status_copia.set_text("Selected files: " + str(cont) + " / " + str(len(self.store)) + " (" + self.get_utils().to_human_size(size) + ") - Videos: " + 
                                         str(cont_video) + " (" + self.get_utils().to_human_size(size_video) + ") - Photos: " + str(cont_photo) + " (" + self.get_utils().to_human_size(size_photo) + ") - Unknown: " + str(cont_outro) + "(" + self.get_utils().to_human_size(size_outro) + ")")

    def do_build_list_files_to_copy(self):
        resp = []
        path_base = self.edit_origem.get_text()
            
        for row in self.store:
            if row[0]:
                resp.append(path_base + os.sep + row[2])
        return resp

    def is_video(self, filename):
        for ext in self.get_app_settings(AppSettings.CFG_VIDEO_EXTENSIONS).split('|'):
            if filename.lower().endswith(ext.lower()):
                return True
        return False

    def is_photo(self, filename):
        for ext in self.get_app_settings(AppSettings.CFG_PHOTO_EXTENSIONS).split('|'):
            if filename.lower().endswith(ext.lower()):
                return True
        return False

    def do_get_photo_list(self, videos):
        resp = []
        for filename in videos:
            if self.is_photo(filename):
                resp.append(filename)
        return resp

    def do_get_video_list(self, files):
        resp = []
        for filename in files:
            if self.is_video(filename):
                resp.append(filename)
        return resp

    def do_click_dir_mapping(self, widget):  # @UnusedVariable
        self.debug("Target dir mapping")

        self._destination_settings.clear()
 
        for filename in self.do_build_list_files_to_copy():
            destino = os.path.dirname(self._destination_settings.get_file_destination(filename))
            self._destination_settings.set_file_destination(destino, os.path.basename(os.path.dirname(filename)))

        if DirMappingDialog(self, self._destination_settings).show_and_update_file_list():
            self.do_build_file_list()

    def do_click_sync_files(self, widget):  # @UnusedVariable
        self.debug("Building the list of files to be copied")

        # Get the selected file list
        files = self.do_build_list_files_to_copy()

        # Filter the photos and videos
        if 'True' == self.get_app_settings(AppSettings.CFG_PHOTO_AND_VIDEO_ONLY):
            self.debug("Filtering only photos and videos")
            medias = self.do_get_photo_list(files)
            medias.extend(self.do_get_video_list(files))
            files = medias

        self.debug("Starting the file copy")
        # Copy the files
        dialog_files = FileCopyProgressDialog(self, files, self.edit_destino.get_text(), self._destination_settings)
        dialog_files.run()
        dialog_files._must_stop = True
        if dialog_files._failed:
            self._show_message("Failed to copy the files!", "There were errors while copying the files, check the log for more information.")

        dialog_files.destroy()
        self.debug("File sync has finished!")

        # Check if it's to re-compress the videos
        if 'True' == self.get_app_settings(AppSettings.CFG_COMPRESS_VIDEO):
            self.debug("Building the list of videos to compress")
            files = self.do_get_video_list(files)
            if len(files) > 0:
                self.debug("Compressing " + str(len(files)) + " video(s).")

                # Save the STDOUT
                saved_stdout = sys.stdout

                # Copy the videos
                dialog_video = VideoEncodeProgressDialog(self, files, self.edit_destino.get_text(), self._destination_settings)
                dialog_video.run()
                # Interrupt the process, if the user hits cancel
                dialog_video._must_stop = True
                if dialog_video._failed:
                    self._show_message("Failed to convert the file", "There were errors while compressing the files, check the log for more information.")

                dialog_video.interrrupt_process()

                dialog_video.destroy()
                self.debug("Video files encoding has finished")

                # Restore the original STDOUT
                sys.stdout = saved_stdout

        self._show_message("Finished!", "File copy operation has finished!")

    def do_click_config(self, widget):  # @UnusedVariable
        self.debug("Setup the application")
        ConfigDialog(self, self._app_settings).show_and_save_setting()

    def do_click_logs(self, widget):  # @UnusedVariable
        self.debug("Show the log viewer")
        viewer = TextViewerDialog(self)
        viewer.set_text(self.get_logger().get_log_text())
        viewer.do_show_window()

    def do_click_close(self, widget):  # @UnusedVariable
        sys.exit(0)

    def on_tree_double_clicked(self, widget, row, col):  # @UnusedVariable
        self.debug("Duble click on the file list (" + str(row) + "," + str(col.get_sort_column_id()) + ")")
        select = self.treeview.get_selection()
        model, treeiter = select.get_selected()
        self.store.set_value(treeiter, 0, not model[treeiter][0])
        self.do_update_selection_counters()

    def get_file_info(self, filename):
    
        capture_info = 'True' == self.get_app_settings(AppSettings.CFG_SHOW_FILE_RESOLUTION)
    
        if not capture_info or not self.is_photo(filename) and not self.is_video(filename):
            return ""

        pattern = re.compile("(Duration: [0-9]{2,}:[0-9]{2,}:[0-9]{2,})|(Video: [^\s]+)|([0-9]{2,}x[0-9]{2,})|([0-9|.]+ fps)|(Audio: [^\s]+)|([0-9]+ Hz)")
        args = [self.get_app_settings(AppSettings.CFG_FFMPEG_PATH), "-hide_banner", "-i", filename]

        self.ffmpeg_process = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, bufsize=1, universal_newlines=True)

        lines = ""
        # Run the process and get all lines
        for line in iter(self.ffmpeg_process.stdout.readline, ''):

            # Discard unnecessary lines
            if line.find("Stream #0") or line.find(" Duration:"):
                lines = lines + line

        if "Duration: 00:00:00" in lines:
            lines = lines.replace("Duration: 00:00:00", "")
            lines = lines.replace("Video: ", "")

        # Get the RegEx groups
        resp = ""
        for m in pattern.finditer(lines):
            resp = resp + m.group() + " "

        # Terminate the process
        self.ffmpeg_process.stdout.close()
        self.ffmpeg_process.wait()

        return resp

