# -*- coding: utf-8 -*-
'''
###############################################################################################'
 BaseContainer.py

 This is a free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 NTLM APS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with the software; see the file COPYING. If not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

###############################################################################################'
 @class Module used as the base for all windows 
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''
from gi.repository import Gtk

from modules.settings.AppSettings import AppSettings
from modules.settings.CodecSettings import CodecSettings
from modules.utils.Logger import Logger
from modules.utils.Utils import Utils
import subprocess
import re

class BaseContainer(object):

    _logger = None
    _app_settings = None
    _codec_settings = None
    _utils = None
    _list_ffmpeg_features = None

    def __del__(self):
        '''
        Close the logger
        '''
        if self._logger is not None:
            self._logger.do_close()
        
    def debug(self, msg=''):
        '''
        Log a debug message
        '''
        self.get_logger().debug(msg)
        
    def get_logger(self):
        '''
        Retrieve the logger
        '''
        if self._logger is None:
            self._logger = Logger() 
        return self._logger

    def get_utils(self):
        
        if self._utils is None:
            self._utils = Utils()
        return self._utils
    
    def get_app_settings(self, xml_tag):
        '''
        Retrieve a system settings
        '''
        if self._app_settings is None:
            self._app_settings = AppSettings()
        return self._app_settings.get_app_settings(xml_tag)
    
    def set_app_settings(self, xml_tag, value):
        '''
        Set a system settings
        '''
        if self._app_settings is None:
            self._app_settings = AppSettings()
        return self._app_settings.set_app_settings(xml_tag, value)
    
    def get_codec_settings(self):
        '''
        Get the list of codec settings
        '''
        if self._codec_settings is None:
            self._codec_settings = CodecSettings(self.get_ffmpeg_features())
        return self._codec_settings

    def get_ffmpeg_features(self):
        
        """
        Retrieve a list of ffmpeg features: Ex: --enable-libx264
        """
        if self._list_ffmpeg_features is None:
            self.ffmpeg_process = subprocess.Popen([self.get_app_settings(AppSettings.CFG_FFMPEG_PATH)], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, bufsize=1, universal_newlines=True)
    
            linhas = ""
            for line in iter(self.ffmpeg_process.stdout.readline, ''):
                if "--" in line:
                    linhas = linhas + line
    
            self.ffmpeg_process.stdout.close()
            self.ffmpeg_process.wait()
    
            list_ffmpeg_features = []
            pattern = re.compile("--enable-[^\s]+|disable-[^\s]+")
            for m in pattern.finditer(linhas):
                list_ffmpeg_features.append(m.group())
    
        return list_ffmpeg_features

    def _show_message(self, title, msg):
        """
        Show a message to the user.
        Note: This method always return None so it can stop the processing

        @param title: The dialog title
        @param msg: The dialog message
        """
    
        self.debug("Message to the user: " + title + ": " + msg)
        # Show a warning
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.INFO, Gtk.ButtonsType.CLOSE, title)
        dialog.format_secondary_text(msg)
        dialog.run()
        dialog.destroy()
        return None
    
    def _ask_question(self, title, msg):
        '''
        Ask a question to the user

        @param title: The dialog title
        @param msg: The dialog message
        '''
        
        self.debug("Question to the user: " + title + ": " + msg)
        # Ask a question
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.QUESTION, Gtk.ButtonsType.YES_NO, title)
        dialog.format_secondary_text(msg)
        response = dialog.run()
        dialog.destroy()
        return response == Gtk.ResponseType.YES
            
    def _create_text_combo(self, options):
        """
        Create a text combo box filled with a list of options.
        
        @param options: The list of text items
        """
    
        # Create the combo
        combo_box = Gtk.ComboBoxText()
        # Add the items
        
        for option in options:
            combo_box.append_text(option)
    
        return combo_box
    
    def _do_hide_and_clear_combo(self, combo, label):
        """
        Clear a combo an hide his relative label.
    
        @param combo: The combo to be cleared
        @param label: The label to be hide  
        """
        combo.remove_all()
        combo.hide()
        label.hide()
    
    def _create_icon_and_label_item(self, label, icon, action):
        """
        Create a MenuItem with an icon

        @param label: The button title
        @param icon: The GTK icon name
        @param action: The button click action method
        """
        self.debug(" - Creating MenuItem: " + label)
        button = Gtk.MenuItem.new()
        b_grid = Gtk.Grid()
        b_grid.set_column_spacing(10)
        b_grid.attach(Gtk.Image.new_from_icon_name(icon, Gtk.IconSize.BUTTON), 0, 0, 1, 1)
        b_grid.attach(Gtk.Label(label=label, halign=Gtk.Align.CENTER), 1, 0, 1, 1)
        b_grid.show_all()
        button.add(b_grid)
        button.connect("activate", action)
        return button
    
    def _create_icon_and_label_button(self, label, icon, action):
        """
        Create a button with an icon

        @param label: The button title
        @param icon: The GTK icon name
        @param action: The button click action method
        """
    
        self.debug(" - Creating button: " + label)
        button = Gtk.Button.new()
        b_grid = Gtk.Grid()
        b_grid.set_column_spacing(10)
        b_grid.set_row_homogeneous(True)
        b_grid.attach(Gtk.Image.new_from_icon_name(icon, Gtk.IconSize.BUTTON), 0, 0, 1, 1)
        b_grid.attach(Gtk.Label(label=label, halign=Gtk.Align.CENTER), 1, 0, 1, 1)
        b_grid.show_all()
        button.add(b_grid)
        button.connect("clicked", action)
        return button
    
    def _compare_tree_rows(self, model, row1, row2, user_data):  # @UnusedVariable
        """
        Sort method used to compare TreeView rows.
        
        @param model: The tree model
        @param row1: The first row  
        @param row2: The second row  
        @param user_data: Tree data  

        """
        sort_column, _ = model.get_sort_column_id()
        value1 = model.get_value(row1, sort_column)
        value2 = model.get_value(row2, sort_column)
    
        if value1 < value2:
            return -1
        elif value1 == value2:
            return 0
        else:
            return 1

class BaseWindow(Gtk.Window, BaseContainer):
    '''
    Base class for GTK Window
    '''
    def __init__(self, **kwds):
        super(BaseWindow, self).__init__(**kwds)
    
class BaseDialog(Gtk.Dialog, BaseContainer):
    '''
    Base class for GTK Dialogs
    '''
    def __init__(self, **kwds):
        super(BaseDialog, self).__init__(**kwds)

