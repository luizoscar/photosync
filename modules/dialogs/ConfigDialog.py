# -*- coding: utf-8 -*-

'''
###############################################################################################'
 ConfigDialog.py

 This is a free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This file is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with the software; see the file COPYING. If not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

###############################################################################################'
 @class Dialog used to configure the application 
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''
from gi.repository import Gtk
import os
from modules.dialogs.BaseContainer import BaseDialog
from modules.dialogs.InputTextFieldDialog import InputTextFieldDialog
from modules.settings.AppSettings import AppSettings
from modules.settings.DestinationDirSettings import DestinationDirFormat

class ConfigDialog(BaseDialog):
    """
    Application config dialog
    """
    
    def __init__(self, parent, codec_settings):
        
        super(ConfigDialog, self).__init__(title= "Application settings",transient_for=parent)
        self.add_button(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL)
        self.add_button(Gtk.STOCK_OK, Gtk.ResponseType.OK)

        self._app_settings = codec_settings
        
        self.set_size_request(400, 300)
        self.set_border_width(10)

        grid = Gtk.Grid()
        grid.set_column_homogeneous(True)
        grid.set_row_homogeneous(True)
        grid.set_column_spacing(2)
        grid.set_row_spacing(2)

        grid_check = Gtk.Grid()

        # Copy only photos and videos
        self.check_photos_videos = Gtk.CheckButton("Copy only photos and videos")
        self.check_photos_videos.set_active('True' == self.get_app_settings(AppSettings.CFG_PHOTO_AND_VIDEO_ONLY))
        grid_check.attach(self.check_photos_videos, 0, 0, 3, 1)

        # Overwrite
        self.check_sobrescrever = Gtk.CheckButton("Overwrite existing files")
        self.check_sobrescrever.set_active('True' == self.get_app_settings(AppSettings.CFG_OVERRIDE_EXISTING))
        grid_check.attach(self.check_sobrescrever, 4, 0, 3, 1)

        # Remove copied files
        self.check_remover_copia = Gtk.CheckButton("Remove the original files after the copy")
        self.check_remover_copia.set_active('True' == self.get_app_settings(AppSettings.CFG_REMOVE_AFTER_COPY))
        grid_check.attach(self.check_remover_copia, 0, 1, 3, 1)

        # Show the file resolution
        self.check_exibir_resolucao = Gtk.CheckButton("Show file resolution (may be slow)")
        self.check_exibir_resolucao.set_active('True' == self.get_app_settings(AppSettings.CFG_SHOW_FILE_RESOLUTION))
        grid_check.attach(self.check_exibir_resolucao, 4, 1, 3, 1)
        
        # Compress videos
        self.check_recode = Gtk.CheckButton("Re-encode the video files")
        self.check_recode.set_active('True' == self.get_app_settings(AppSettings.CFG_COMPRESS_VIDEO))
        grid_check.attach(self.check_recode, 0, 2, 3, 1)

        # Video format
        flowbox = Gtk.FlowBox()
        flowbox.set_selection_mode(Gtk.SelectionMode.NONE)

        flowbox.add(Gtk.Label(label="Video format:", halign=Gtk.Align.START))
        self.combo_codecs = self._create_text_combo(self.get_codec_settings().get_video_codec_names())
            
        self.combo_codecs.set_active(0)
        self.combo_codecs.set_entry_text_column(1)
        codec_idx = self.get_app_settings(AppSettings.CFG_VIDEO_CODEC)
        if codec_idx is not None:
            self.combo_codecs.set_active(int(codec_idx))
            
        flowbox.add(self.combo_codecs)

        grid_check.attach(flowbox, 4, 2, 3, 1)

        # Remove converted videos
        self.check_remover_video = Gtk.CheckButton("Remove the original copied video file after compression")
        self.check_remover_video.set_active('True' == self.get_app_settings(AppSettings.CFG_REMOVE_SOURCE_AFTER_COMPRESSION))
        grid_check.attach(self.check_remover_video, 0, 3, 3, 1)

        grid.attach(grid_check, 0, 0, 6, 3)

        # Target dir
        self.edit_caminho_ffmpeg = Gtk.Entry()
        self.edit_caminho_ffmpeg.set_text(self.get_app_settings(AppSettings.CFG_FFMPEG_PATH))

        button = Gtk.Button.new_from_icon_name("document-open", Gtk.IconSize.BUTTON)
        button.connect("clicked", self.do_click_seleciona_ffmpeg)

        box_destino = Gtk.Box()
        box_destino.pack_start(Gtk.Label(label="Path to ffmpeg:", halign=Gtk.Align.START), False, False, 0)
        box_destino.pack_start(self.edit_caminho_ffmpeg, True, True, 4)
        box_destino.pack_end(button, False, False, 0)

        grid.attach(box_destino, 0, 3, 7, 1)
        
        # Destination dir
        flowbox = Gtk.FlowBox()
        flowbox.set_selection_mode(Gtk.SelectionMode.NONE)

        flowbox.add(Gtk.Label(label="Destination sub-dir:", halign=Gtk.Align.START))

        self.combo_destino = self._create_text_combo(DestinationDirFormat.DESTINATION_DIR_LIST)
        self.combo_destino.set_active(0)
        self.combo_destino.set_entry_text_column(1)
        dir_idx = self.get_app_settings(AppSettings.CFG_NEW_DIR_FORMAT)
        if dir_idx is not None:
            self.combo_destino.set_active(int(dir_idx))
        
        flowbox.add(self.combo_destino)

        grid.attach(flowbox, 0, 4, 6, 1)
        
        grid.attach(Gtk.Label(label="File classification:", halign=Gtk.Align.START), 0, 5, 6, 1)

        # Video list
        self.taskstore_videos = Gtk.ListStore(str)
        self.treeview_videos = Gtk.TreeView(model=self.taskstore_videos)
        self.treeview_videos.append_column(Gtk.TreeViewColumn("Video file extensions", Gtk.CellRendererText(), text=0))

        scrollable_treelist_videos = Gtk.ScrolledWindow()
        scrollable_treelist_videos.set_vexpand(True)
        scrollable_treelist_videos.set_hexpand(True)
        scrollable_treelist_videos.add(self.treeview_videos)

        grid_video = Gtk.Grid()
        grid_video.attach(scrollable_treelist_videos, 0, 0, 6, 6)

        for video in self.get_app_settings(AppSettings.CFG_VIDEO_EXTENSIONS).split('|'):
            self.taskstore_videos.append([video])

        flowbox = Gtk.FlowBox()
        flowbox.set_selection_mode(Gtk.SelectionMode.NONE)
        button = Gtk.Button.new_from_icon_name("list-add", Gtk.IconSize.MENU)
        button.connect("clicked", self.do_click_add_video)
        flowbox.add(button)
        grid_video.attach(flowbox, 7, 3, 1, 1)

        flowbox = Gtk.FlowBox()
        flowbox.set_selection_mode(Gtk.SelectionMode.NONE)
        button = Gtk.Button.new_from_icon_name("list-remove", Gtk.IconSize.MENU)
        button.connect("clicked", self.do_click_del_video)
        flowbox.add(button)
        grid_video.attach(flowbox, 7, 4, 1, 1)

        grid.attach(grid_video, 0, 6, 3, 6)

        # Photo list
        self.taskstore_photos = Gtk.ListStore(str)
        self.treeview_photos = Gtk.TreeView(model=self.taskstore_photos)
        self.treeview_photos.append_column(Gtk.TreeViewColumn('Photo file extension', Gtk.CellRendererText(), text=0))

        scrollable_treelist_photos = Gtk.ScrolledWindow()
        scrollable_treelist_photos.set_vexpand(True)
        scrollable_treelist_photos.set_hexpand(True)
        scrollable_treelist_photos.add(self.treeview_photos)

        grid_photo = Gtk.Grid()
        grid_photo.attach(scrollable_treelist_photos, 0, 0, 6, 6)

        for photo in self.get_app_settings(AppSettings.CFG_PHOTO_EXTENSIONS).split('|'):
            self.taskstore_photos.append([photo])

        flowbox = Gtk.FlowBox()
        flowbox.set_selection_mode(Gtk.SelectionMode.NONE)
        button = Gtk.Button.new_from_icon_name("list-add", Gtk.IconSize.MENU)
        button.connect("clicked", self.do_click_add_photo)
        flowbox.add(button)

        grid_photo.attach(flowbox, 7, 3, 1, 1)

        flowbox = Gtk.FlowBox()
        flowbox.set_selection_mode(Gtk.SelectionMode.NONE)
        button = Gtk.Button.new_from_icon_name("list-remove", Gtk.IconSize.MENU)
        button.connect("clicked", self.do_click_del_photo)
        flowbox.add(button)
        grid_photo.attach(flowbox, 7, 4, 1, 1)

        grid.attach(grid_photo, 4, 6, 3, 6)

        self.get_content_area().pack_start(grid, False, False, 0)
        self.show_all()

    def do_click_seleciona_ffmpeg(self, widget):  # @UnusedVariable
        self.debug("Selecting the ffmpeg path")

        dialog = Gtk.FileChooserDialog('Select the path to the ffmpeg application', self, Gtk.FileChooserAction.OPEN,
                                       (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OPEN, Gtk.ResponseType.OK))

        caminho = self.edit_caminho_ffmpeg.get_text().strip()
        if os.path.isfile(caminho):
            dialog.set_current_folder(caminho)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.edit_caminho_ffmpeg.set_text(dialog.get_filename())
            self.debug("Path to the ffmpeg: " + dialog.get_filename())

        dialog.destroy()

    def do_click_del_video(self, widget):  # @UnusedVariable
        self.remove_item("video")

    def do_click_add_video(self, widget):  # @UnusedVariable
        self.add_item("video")

    def do_click_del_photo(self, widget):  # @UnusedVariable
        self.remove_item("photo")

    def do_click_add_photo(self, widget):  # @UnusedVariable
        self.add_item("photo")

    def add_item(self, titulo):
        info = InputTextFieldDialog(self.parent, 'New ' + titulo+' file extension:').do_show_and_get_value()
        if info is not None:
            store = self.taskstore_videos if titulo == "video" else self.taskstore_photos
            store.append([info])

    def remove_item(self, titulo):
        self.debug("Removing item from the list of " + titulo)
        tree = self.treeview_photos
        store = self.taskstore_photos
        if titulo == "video":
            store = self.taskstore_videos
            tree = self.treeview_videos

        select = tree.get_selection()
        treeiter = select.get_selected()

        if treeiter[1] is None:
            return self._show_message('Unable to remove extension', "You need to select one item to continue.")

        store.remove(treeiter)

    def show_and_save_setting(self):
        while self.run() == Gtk.ResponseType.OK:
            self.set_app_settings(AppSettings.CFG_REMOVE_AFTER_COPY, str(self.check_remover_copia.get_active()))
            self.set_app_settings(AppSettings.CFG_OVERRIDE_EXISTING, str(self.check_sobrescrever.get_active()))
            self.set_app_settings(AppSettings.CFG_COMPRESS_VIDEO, str(self.check_recode.get_active()))
            self.set_app_settings(AppSettings.CFG_FFMPEG_PATH, self.edit_caminho_ffmpeg.get_text().strip())
            self.set_app_settings(AppSettings.CFG_VIDEO_CODEC, str(self.combo_codecs.get_active()))
            self.set_app_settings(AppSettings.CFG_PHOTO_AND_VIDEO_ONLY, str(self.check_photos_videos.get_active()))
            self.set_app_settings(AppSettings.CFG_SHOW_FILE_RESOLUTION, str(self.check_exibir_resolucao.get_active()))
            self.set_app_settings(AppSettings.CFG_NEW_DIR_FORMAT, str(self.combo_destino.get_active()))                                       

            videos = ""
            for row in self.taskstore_videos:
                videos = videos + "|" + row[0]
            videos = videos[1:]
            self.set_app_settings(AppSettings.CFG_VIDEO_EXTENSIONS, videos)

            photo = ""
            for row in self.taskstore_photos:
                photo = photo + "|" + row[0]
            photos = photo[1:]
            self.set_app_settings(AppSettings.CFG_PHOTO_EXTENSIONS, photos)
            self.destroy()
            return True

        self.destroy()
        return False
