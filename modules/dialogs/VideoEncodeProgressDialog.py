# -*- coding: utf-8 -*-

'''
###############################################################################################'
 VideoEncodeProgressDialog.py

 This is a free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This file is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with the software; see the file COPYING. If not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

###############################################################################################'
 @class Dialog used to show the video conversion process 
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''
from __future__ import division

import os
import time
import datetime
import subprocess
from threading import Thread

from gi.repository import Gtk,GLib

from modules.dialogs.BaseContainer import BaseDialog
from modules.settings.CodecSettings import CodecInfo
from modules.settings.AppSettings import AppSettings

class VideoEncodeProgressDialog(BaseDialog):
    """
    Dialog used to show the video conversion process
    """
    
    _total = 0
    _completed_size = 0
    _must_stop = False
    _failed = False
    _ffmpeg_process = None

    def __init__(self, parent, file_list, destination_dir, destination_settings):
        
        super(VideoEncodeProgressDialog, self).__init__(title="Compressing videos", transient_for=parent)
        self.add_button(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL)

        self.set_size_request(250, 150)
        self.set_border_width(10)

        self.file_list = file_list
        self.destination_dir = destination_dir
        self.destination_settings = destination_settings

        # Main container
        grid = Gtk.Grid()
        grid.set_column_homogeneous(True)
        grid.set_row_homogeneous(True)
        grid.set_column_spacing(4)
        grid.set_row_spacing(6)

        for filename in self.file_list:
            self._total = self._total + os.stat(filename).st_size

        # Activity label
        grid.attach(Gtk.Label(label="Re-encoding " + str(len(file_list)) + 
                              " files (" + self.get_utils().to_human_size(self._total) + ")", halign=Gtk.Align.START), 0, 0, 6, 1)

        # Total progress
        self.progress_bar_total = Gtk.ProgressBar(show_text=True)
        grid.attach(self.progress_bar_total, 0, 1, 6, 1)

        # Global progress title
        self.label_progress_total = Gtk.Label(halign=Gtk.Align.START)
        grid.attach(self.label_progress_total, 0, 2, 6, 1)

        # File conversion progress
        self.progress_bar_current = Gtk.ProgressBar(show_text=True)
        grid.attach(self.progress_bar_current, 0, 3, 6, 1)

        # File name title
        self.label_current = Gtk.Label(halign=Gtk.Align.START)
        grid.attach(self.label_current, 0, 4, 6, 1)

        self.get_content_area().pack_start(grid, True, True, 0)
        self.show_all()

        # Start the conversion thread
        thread = Thread(target=self.convert_videos)
        thread.daemon = True
        thread.start()

    def _do_update_progess(self, total_bar_title, total_progress, total_label_title, current_label_title):
        """        
        Update the file copy progress
        """
        self.progress_bar_total.set_text(total_bar_title)
        self.progress_bar_total.set_fraction(total_progress)  # Progress must be between 0.0 and 1.0
        self.label_progress_total.set_text(total_label_title)
        self.label_current.set_text(current_label_title)

        return False

    def _update_progess_current_file(self, progress):
        """
        Update the file progress bar
        """
        self.progress_bar_current.set_fraction(progress)  # Progress must be between 0.0 and 1.0
        return False

    def convert_videos(self):
        """
        Convert the video
        """
        
        DURATION = "Duration:"
        FRAME = "frame="
        TIME = "time="

        # Get the codec info
        codec_idx = self.get_app_settings(AppSettings.CFG_VIDEO_CODEC)
        codec_idx = codec_idx if codec_idx is not None else "0"
        codec_name = self.get_codec_settings().get_video_codec_names()[int(codec_idx)]
        codec_info = self.get_codec_settings().get_codec_info(codec_name)
        for video_file in self.file_list:
            try:

                if not os.path.isfile(video_file):
                    self.debug("Ignoring missing file: " + video_file)
                    self._failed = True
                    continue

                self._completed_size = self._completed_size + os.stat(video_file).st_size
                new_file = self.destination_dir + os.sep + self.destination_settings.get_file_destination(video_file)

                # Build the ffmpeg command line
                args = [self.get_app_settings(AppSettings.CFG_FFMPEG_PATH), "-hide_banner", "-i", video_file]
                args.extend(codec_info.get_settings(CodecInfo.PARAMS))
                new_file = new_file[:new_file.rindex('.')] + codec_info.get_settings(CodecInfo.SUFFIX)
                args.append(new_file)

                # Get the conversion progress
                title_total_bar = "[" + self.get_utils().to_human_size(self._completed_size) + "/" + self.get_utils().to_human_size(self._total) + "]"
                title_total_label = "Source: " + os.path.basename(video_file) + " (" + self.get_utils().to_human_size(os.stat(video_file).st_size) + ")"

                title_current_label = "Compressed: " + os.path.basename(new_file)
                
                total_progress = self._completed_size / self._total  # Progress


                # Update the counters
                GLib.idle_add(self._do_update_progess, title_total_bar, total_progress, title_total_label, title_current_label)

                # Create the destination dir
                directory = os.path.dirname(new_file)
                if not os.path.exists(directory):
                    self.debug("Creating directory " + directory)
                    os.makedirs(directory)

                # Check if the destination file exists
                if os.path.isfile(new_file):
                    self.debug("Removing existing destination file: " + new_file)
                    os.remove(new_file)

                max_secs = 0
                cur_secs = 0

                # Check if the user pressed cancel
                if self._must_stop:
                    return None

                # Convert the file
                self.debug("Running application: " + str(args))

                self._ffmpeg_process = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, bufsize=1, universal_newlines=True)

                # Run the process and iterate on the output lines
                for line in iter(self._ffmpeg_process.stdout.readline, ''):
                    if DURATION in line:
                        # This line has the video duration
                        try:
                            tmp = line[line.find(DURATION):]
                            tmp = tmp[tmp.find(" ") + 1:]
                            tmp = tmp[0: tmp.find(".")]
                            x = time.strptime(tmp, '%H:%M:%S')
                            max_secs = datetime.timedelta(hours=x.tm_hour, minutes=x.tm_min, seconds=x.tm_sec).total_seconds()
                        except ValueError:
                            self.debug("Failed to convert the time: " + tmp)

                    elif line.startswith(FRAME) and TIME in line:
                        try:
                            # Get the conversion time (timestamp)
                            tmp = line[line.find(TIME):]
                            tmp = tmp[tmp.find("=") + 1: tmp.find(".")]
                            x = time.strptime(tmp, '%H:%M:%S')
                            cur_secs = datetime.timedelta(hours=x.tm_hour, minutes=x.tm_min, seconds=x.tm_sec).total_seconds()
                        except ValueError:
                            self.debug("Failed to convert the time: " + tmp)

                    # Update the progress
                    if cur_secs > 0 and max_secs > 0:
                        GLib.idle_add(self._update_progess_current_file, cur_secs / max_secs)

                # Close the process
                self._ffmpeg_process.stdout.close()
                self._ffmpeg_process.wait()

                if os.path.isfile(video_file):
                    self.debug("Source video: " + video_file + " (" + self.get_utils().to_human_size(os.stat(video_file).st_size) + ")")

                if os.path.isfile(new_file):
                    self.debug("Converted video: " + new_file + " (" + self.get_utils().to_human_size(os.stat(new_file).st_size) + ")")

                # Remove the original file
                if 'True' == self.get_app_settings(AppSettings.CFG_REMOVE_SOURCE_AFTER_COMPRESSION):
                    video_original = os.path.dirname(new_file) + os.sep + os.path.basename(video_file)
                    if os.path.isfile(video_original):
                        self.debug("Removing the original source file: " + video_original)
                        os.remove(video_original)

            except Exception as e:
                self.debug("Failed to convert the video file " + video_file + " : "+ str(e))
                self._failed = True

        self.close()


    def interrrupt_process(self):
        '''
        Make sure ffmpeg process is finished!
        '''    
        if self._ffmpeg_process is not None:
            try:
                self._ffmpeg_process.kill()
                self.debug("ffmpeg process was interrupted by the user.")
            except OSError:
                self.debug("The ffmpeg process was successfully terminated.")