# -*- coding: utf-8 -*-
'''
###############################################################################################'
 AppSettings.py

 This is a free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This file is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with the software; see the file COPYING. If not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

###############################################################################################'
 @class Module that saves and retrieve the application settings
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''

import os

from modules.settings.BaseXmlManager import BaseXmlManager

class AppSettings(BaseXmlManager):
    '''
    Module to set/get the system settings. 
    '''

    # Tags used by the settings XML file.
    TAG_CONFIG = 'config'

    CFG_TARGET_DIR = 'source_dir'
    CFG_SOURCE_DIR='target_dir'
    CFG_VIDEO_EXTENSIONS='video_extensions'
    CFG_PHOTO_EXTENSIONS='photo_extensions'
    CFG_VIDEO_CODEC='video_codec'
    CFG_FFMPEG_PATH='ffmpeg_path'
    CFG_NEW_DIR_FORMAT='new_dir_format'
    CFG_PHOTO_AND_VIDEO_ONLY = 'copy_photo_and_video_only'
    CFG_OVERRIDE_EXISTING = 'override_existing_files'
    CFG_REMOVE_AFTER_COPY = 'remove_source_after_copy'
    CFG_SHOW_FILE_RESOLUTION = 'show_file_resolution'
    CFG_COMPRESS_VIDEO = 'compress_videos'
    CFG_REMOVE_SOURCE_AFTER_COMPRESSION = 'remove_source_video_after_compression'

    # Application dir
    _APP_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
    
    def __init__(self):
        '''
        Default constructor
        '''
        super(AppSettings, self).__init__(self._APP_DIR + os.sep + "settings.xml")
        # Create the settings with the default values
        if not os.path.isfile(self.xml_file):
            self.set_app_settings(self.CFG_TARGET_DIR, str(os.path.expanduser('~')))
            self.set_app_settings(self.CFG_SOURCE_DIR, str(os.path.expanduser('~')))
            self.set_app_settings(self.CFG_VIDEO_EXTENSIONS, "wmv|avi|mpg|3gp|mov|m4v|mts|mp4")
            self.set_app_settings(self.CFG_PHOTO_EXTENSIONS, "dof|arw|raw|jpg|jpeg|png|nef")
            self.set_app_settings(self.CFG_VIDEO_CODEC, "0")
            self.set_app_settings(self.CFG_FFMPEG_PATH, "ffmpeg")
            self.set_app_settings(self.CFG_NEW_DIR_FORMAT, "0")
    
    def set_app_settings(self, name, value):
        """
        Set an application settings
        
        @param name: The settings name
        @param value: The settings value
        """
        root_node = self.get_root_node() if os.path.isfile(self.xml_file) else self.create_element(AppSettings.TAG_CONFIG)
    
        # If the value is not None, create a new node
        if value is not None and str(value).strip():
            self.create_unique_element(name, root_node).text = self.get_utils().unicode(value)
        else:
            root_node = self.remove_element(name)
    
        self.save(root_node)
    
    def get_app_settings(self, xml_tag):
        """
        Get an application settings
        
        @param xml_tag: The XML attribute name
        """        
        settings_node = self.get_element_list("./" + xml_tag)
        return None if not settings_node else self.get_utils().unicode(settings_node[0].text).strip()

    def set_debug_mode(self, debug_mode):
        '''
        Set the application debug mode
        
        @param debug_mode: The application debug mode
        '''
        self.set_app_settings(AppSettings.CFG_DEBUG_MODE, str(debug_mode))
    
    def is_debug_mode(self):
        '''
        Check if the application is in debug mode
        '''
        return 'True' == self.get_app_settings(AppSettings.CFG_DEBUG_MODE)
