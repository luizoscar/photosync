# -*- coding: utf-8 -*-
'''
###############################################################################################'
 DestinationDirSettings.py

 This is a free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This file is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with the software; see the file COPYING. If not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

###############################################################################################'
 @class Module to retrieve the application CODEC details.
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''

import os
import datetime
from modules.settings.AppSettings import AppSettings

class DestinationDirFormat:
    '''
    Destination dir format
    '''
    DST_YYYY_YYYYMMDD = "Create with the format yyyy/yyyy-mm-dd/"
    DST_YYYYMMDD = "Create with the format yyyy-mm-dd/"
    DST_ORIGINAL = "Create a dir with the same name as the source dir"
    DST_DO_NOT_CREATE = "Do not create sub-directory"

    DESTINATION_DIR_LIST = (DST_YYYY_YYYYMMDD, DST_YYYYMMDD, DST_ORIGINAL, DST_DO_NOT_CREATE)
    
class DestinationDirSettings(object):
    '''
    Class used to store the destination dirs
    '''

    _dic_map_dir_target = {} # Target dir mapping
    _dic_map_dir_source = {}  # Source dir mapping

    def __init__(self):
        '''
        Default constructor
        '''
        self._app_settings = AppSettings()
    
    def clear(self):
        '''
        Clear the destinations
        '''
        self._dic_map_dir_target = {}
        self._dic_map_dir_source = {}
    
    def get_file_destination(self, media_file):
        '''
        Get the destination for the file
        @param media_file: The video / photo file 
        '''
    
        self._dic_map_dir_target = {} if self._dic_map_dir_target is None else self._dic_map_dir_target 
        
        nome = os.path.basename(media_file)
        data = datetime.datetime.fromtimestamp(os.path.getmtime(media_file))
    
        dir_idx = self._app_settings.get_app_settings(AppSettings.CFG_NEW_DIR_FORMAT)
        
        dst = DestinationDirFormat.DESTINATION_DIR_LIST[int(dir_idx)] if dir_idx else DestinationDirFormat.DESTINATION_DIR_LIST[0]
            
        # Build the destination dir name based on the settings
        if dst == DestinationDirFormat.DST_YYYY_YYYYMMDD:
            # Target: /YYYY/yyyy-MM-dd/file
            destino = str(data.year) + os.sep + str(data.year) + "-" + str(data.month).zfill(2) + "-" + str(data.day).zfill(2)
        elif dst == DestinationDirFormat.DST_YYYYMMDD:
            # Target: /yyyy-MM-dd/file
            destino = str(data.year) + "-" + str(data.month).zfill(2) + "-" + str(data.day).zfill(2)
        elif dst == DestinationDirFormat.DST_ORIGINAL:
            # Original dir name
            destino = os.path.basename(os.path.dirname(media_file))
        else:
            destino = ""
        
        if destino in self._dic_map_dir_target:
            destino = self._dic_map_dir_target[destino]
        
        return destino + os.sep + nome if destino else nome
    
    def set_file_destination(self, destination, filename):
        '''
        Set the file destination
        '''   
        self._dic_map_dir_target[destination] = destination
        self._dic_map_dir_source[destination] = filename 
        
    def get_source_mapping(self):
        '''
        Get the source file mapping
        '''
        return self._dic_map_dir_target
    
    def get_target_mapping(self):
        '''
        Get the destination file mapping
        '''
        return self._dic_map_dir_target
