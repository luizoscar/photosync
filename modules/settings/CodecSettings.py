# -*- coding: utf-8 -*-
'''
###############################################################################################'
 CodecSettings.py

 This is a free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This file is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with the software; see the file COPYING. If not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

###############################################################################################'
 @class Module to retrieve the application CODEC details.
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''
from modules.settings.BaseXmlManager import BaseXmlManager
import os


class CodecInfo(object):
    '''
    Class used to represent a CODEC info.
    '''

    NAME = 'name'
    SUFFIX = 'suffix'
    REQUIREMENT = 'requirement'
    PARAMS = 'params'
    
    def __init__(self):
        '''
        Default constructor
        '''
        self._codec_settings = {}
    
    def get_settings(self, name):
        '''
        Get the CODEC settings
        '''
        return self._codec_settings[name]
    
    def set_settings(self, name, value):
        '''
        Set the CODEC settings
        '''
        self._codec_settings[name] = value


class CodecSettings(BaseXmlManager):
    '''
    Class used to load the list of CODEC details.
    '''
    _list_ffmpeg_features = None  # List with ffmpeg features

    def __init__(self, ffmpeg_features):
        '''
        Default constructor
        '''
        super(CodecSettings, self).__init__(os.path.dirname(os.path.realpath(__file__)) + os.sep + "codecs.xml")
        self._list_ffmpeg_features = ffmpeg_features

        self._audio_codecs = self._build_codec_list('audio')
        self._video_codecs = self._build_codec_list('video')
        self._extra_codecs = self._build_codec_list('extra')
        
    def _build_codec_list(self, codec_type):
        '''
        Build the list of codecs
        
        @param codec_type: The codec type (audio / video) 
        '''
        resp = {}
        for element in self.get_element_list('./{}/codec'.format(codec_type)):
            codec = CodecInfo()
            name = self.get_att_value(element, 'name')
            codec.set_settings(CodecInfo.NAME, name)
            codec.set_settings(CodecInfo.SUFFIX, self.get_att_value(element, 'file_suffix'))
            requires = self.get_att_value(element, 'requires')
            codec.set_settings(CodecInfo.REQUIREMENT, requires)
            
            self._list_ffmpeg_features
            
            params = []
            
            for param_elm in self.get_element(element, 'params'):
                params.append(self.get_att_value(param_elm, 'value'))
            
            codec.set_settings(CodecInfo.PARAMS, params)

            if not requires or requires in self._list_ffmpeg_features: 
                resp[name] = codec
                
        return resp
    
    def get_audio_codec_names(self):
        '''
        Get the list of names from the audio codecs
        '''
        resp = []
        for codec in self._audio_codecs:
            resp.append(self._audio_codecs[codec].get_settings(CodecInfo.NAME)) 
        
        return resp
    
    def get_video_codec_names(self):
        '''
        Get the list of names from the video codecs
        '''
        resp = []
        for codec in self._video_codecs:
            resp.append(self._video_codecs[codec].get_settings(CodecInfo.NAME)) 
        
        return resp

    def get_extra_codec_names(self):
        '''
        Get the list of names from the extra codecs
        '''
        resp = []
        for codec in self._extra_codecs:
            resp.append(self._extra_codecs[codec].get_settings(CodecInfo.NAME)) 
        
        return resp
    
    def get_all_codec_names(self):
        '''
        Get the list with all codec names.
        '''
        resp = self.get_video_codec_names()
        resp.extend(self.get_audio_codec_names())
        resp.extend(self.get_extra_codec_names())
        return resp
    
    def get_codec_info(self, name):
        '''
        Get the codec info by Name
        '''
        resp = self._audio_codecs.get(name,None)
        if resp is None:
            resp = self._video_codecs.get(name,None)
        if resp is None:
            resp = self._extra_codecs.get(name,None)
            
        return resp