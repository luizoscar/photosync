# -*- coding: utf-8 -*-

'''
###############################################################################################'
 Utils.py

 This is a free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This file is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with the software; see the file COPYING. If not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

###############################################################################################'
 @class General utility class
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''
import math
import time
import sys

class Utils(object):
    '''
    General utility class
    '''
    SIZE_UNITY = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']

    def to_human_size(self, nbytes):
        """
        Convert from bytes to SI format
        
        @param nbytes: The number of bytes 
        """
        
        human = nbytes
        rank = 0
        if nbytes != 0:
            rank = int((math.log10(nbytes)) / 3)
            rank = min(rank, len(self.SIZE_UNITY) - 1)
            human = nbytes / (1024.0 ** rank)
        f = ('%.2f' % human).rstrip('0').rstrip('.')
        return '%s %s' % (f, self.SIZE_UNITY[rank])

    def seconds_to_time(self, secs):
        """
        Convert a time in seconds to HH:MM:SS
        
        @param secs: The number of seconds 
        """
    
        return time.strftime('%H:%M:%S', time.gmtime(secs))
    
    def time_to_seconds(self, time):
        """
        Convert a time in the format HH:MM:SS to seconds
        
        @param time: The timestamp
        """
        
        ftr = [3600, 60, 1]
        try:
            return sum([a * b for a, b in zip(ftr, map(int, time.split(':')))])
        except ValueError:
            return 0
    
    
    def decode_utf8(self, text):
        if sys.version_info < (3, 0):
            return None if text is None else text.decode("utf-8")
        else:
            return None if text is None else str(text)

    def unicode(self, text):
        if sys.version_info < (3, 0):
            return None if text is None else unicode(text)
        else:
            return None if text is None else str(text)