# -*- coding: utf-8 -*-

'''
###############################################################################################'
 Logger.py

 This is a free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This file is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with the software; see the file COPYING. If not, write to the
 Free Software Foundation, Inc.,
 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

###############################################################################################'
 @class Application logger
 @author: Luiz Oscar Machado Barbosa - <luizoscar@gmail.com>
##############################################################################################
'''

import logging
import os


class Logger(object):
    '''
    Logging module
    '''

    # Log file
    _LOG_FILE = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))) + os.sep + "application.log"
    _LOGGER_NAME = '-'

    def __init__(self):
        '''
        Default constructor.
        '''
        
        # NOTE: Logger should be loaded just after the log file was deleted 
        logging.basicConfig(level=logging.INFO, format='%(asctime)-15s %(message)s')
        self.loger = logging.getLogger(self._LOGGER_NAME)
        self.log_handler = logging.FileHandler(self._LOG_FILE)
        self.loger.addHandler(self.log_handler)

    def __del__(self):
        '''
        Default destructor
        '''
        self.do_close()
        
    def debug(self, msg=''):
        """
        Log a message
        
        @param msg: The message to be logged
        """
        self.loger.info(str(msg).strip())   
        
    def do_close(self):
        '''
        Close the log FileHandler and release the file 
        '''
        self.log_handler.close()
        self.loger.removeHandler(self.log_handler)
        
    def get_log_text(self):
        '''
        Get the text from the log file 
        '''
        return open(self._LOG_FILE).read()
